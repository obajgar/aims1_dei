import numpy as np

def mean_intercepts(x, f):
    """
    Finds points where the series f crosses its mean from below and
    returns the corresponding elements of x
    Arguments:
        x, f: numeric arrays of same length
    Returns:
        Array of means corresponding to the middle of the x interval
        corresponding to f intercepting its mean
    """
    intercepts = []
    f_mean = np.mean(f)
    last_f = f_mean + 1 # anything above the mean

    for x_val, f_val in zip(x, f):
      if last_f <= f_mean and f_val > f_mean:
        intercepts.append(np.mean([x_val,last_x]))
      last_f = f_val
      last_x = x_val

    intercepts = np.array(intercepts)

    return intercepts
