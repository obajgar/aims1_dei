import plotly.graph_objects as go

def plot_tide_heights(th_df):

  fig = go.Figure()

  fig.add_trace(go.Scatter(x=th_df['t'], y=th_df['y_pred']+2*th_df['sd'], fill=None, mode='lines', line_color='yellow', line_width=1, showlegend=False))
  fig.add_trace(go.Scatter(x=th_df['t'], y=th_df['y_pred']-2*th_df['sd'], fill='tonexty', mode='lines',line_color='yellow', line_width=1, name='2 σ'))


  fig.add_trace(go.Scatter(x=th_df['t'], y=th_df['y_pred']+th_df['sd'], fill=None, mode='lines', line_color='orange', line_width=1, showlegend=False))
  fig.add_trace(go.Scatter(x=th_df['t'], y=th_df['y_pred']-th_df['sd'], fill='tonexty', mode='lines',line_color='orange', line_width=1, name='1 σ'))


  fig.add_trace(go.Scatter(x=th_df['t'], y=th_df['y_pred'], mode='lines', name='Mean predicted tide height', line_width=1, line_color='red'))
  fig.add_trace(go.Scatter(x=th_df['t'], y=th_df['y'], mode='markers', name='Observed tide height', marker_size=3, marker_color='blue'))

  fig.update_xaxes(title_text="t (minutes)")
  fig.update_yaxes(title_text="y (m)")
  fig.update_layout(showlegend=True)

  return fig
