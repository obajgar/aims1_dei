import numpy as np

def constant_mean_gen(y):
  # y: a real valued vector
  # returns: a function, which for any input vector x gives a mean(y)-valued
  #          vector of the same length as x
  mu = np.mean(y)
  return lambda x: len(x) * [mu]


def periodic_mean_gen(period, amplitude, voffset, hoffset):
    # Generates a periodic-prior function with the above hyperparameters.

    def periodic_prior(x):
        y_pred = amplitude * np.sin(2*np.pi*(x-hoffset)/period) + voffset
        return y_pred

    return periodic_prior
