import numpy as np

def rbf_cov_gen(lamb, omega):
    """
    Generates an RBF-based covariance function with hyperparameters lamb and
    omega

    Arguments:
        lamb : float -the "height" hyperparameter.
        omega : float
            the "width" hyperparameter.

    Returns
        function - an RBF covariance function with the given hyperparameters.

    """
    def rbf_cov(x):
        """
        RBF covariance function
        arguments:
            x: array - numpy array of n elements from which a covariance matrix
                     should be generated

        returns:
            K: an n x n covariance matrix
        """
        # Matrix of pairwise distances
        dist = np.abs(x[:,None]-x[None,:])
        # Matrix of periodic covariances based on the distances
        K = lamb**2 * np.exp(-(dist/omega)**2/2)
        return K
    return rbf_cov

def periodic_cov_gen(rho, lamb=1., omega=1.):
    """
    Generates an RBF-based covariance function with hyperparameters lamb and
    omega

    Arguments:
        lamb : float -the "height" hyperparameter.
        omega : float
            the "width" hyperparameter.

    Returns
        function - an RBF covariance function with the given hyperparameters.

    """

    def periodic_cov(x):
        """
          periodic covariance function
          arguments:
          x: array - numpy array of n elements from which a covariance matrix
                     should be generated

          returns:
            K: an n x n covariance matrix
        """
        # Matrix of pairwise distances
        dist = np.abs(x[:,None]-x[None,:])
        # Matrix of periodic covariances based on the distances
        K = lamb**2 * np.exp(-2*(np.sin(np.pi*dist/rho))**2/omega)
        return K
    return periodic_cov
