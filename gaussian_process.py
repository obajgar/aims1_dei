import numpy as np
import numpy.linalg as la

def gp(x_test, x, y, mean_fun, cov_fun, noise_sd):
    """
      An implementation of a 1-D input, 1-D output Gaussian process.

      Arguments:
        x_test: array - values of the independent variable at which estimates should
                        be made
        x: array - values of the independent variable corresponding to the provided
                    observations
        y: array - observations at points x
        mean_fun: function - prior mean function taking a vector and providing
                                  a vector of prior mean estimates corresponding to
                                  each values
        cov_fun: function - prior covariance function
        noise_sd: float - standard deviation of the observation noise

        Returns:
            mu_f_pred - array of means of the estimate for each x_test
            var_f_pred - the covariance matrix of the estimates
    """

    n_test = len(x_test)
    n_data = len(x)
    assert len(y) == n_data

    # We assume cov_fun is already able to take a vector as an argument and returns a matrix
    K = cov_fun(np.concatenate((x_test,x)))
    # Prior variance of the test estimates
    k_test = K[0:n_test,0:n_test]
    # Covariance matrix of the data
    K_data = K[n_test:,n_test:]

    # Calculate the Cholesky decomposition and its inverse
    L = la.cholesky(K_data+noise_sd**2*np.identity(n_data))
    L_inv = la.inv(L)

    # Deviation of the observed data relative to the prior mean
    y_mean = mean_fun(x)
    y_rel = y-y_mean

    # Mean of the predicted points
    mu_f_pred = mean_fun(x_test) + \
                  np.matmul(K[:n_test, n_test:], np.matmul(np.transpose(L_inv),
                                                 np.matmul(L_inv, y_rel)))
    # Covariance of the predicted data
    var_f_pred = k_test - np.matmul(K[:n_test, n_test:], np.matmul(np.transpose(L_inv),
                          np.matmul(L_inv, K[n_test:, :n_test])))

    return mu_f_pred, var_f_pred


def mnll(y, mu, K, noise_sd=0.1):
    """
    Calculates the negative log likelihood of data y under
    a multivariate Gaussian with mean mu and variance K+noise_sd^2
    """
    vars = np.diag(K)+noise_sd
    ll = 0.5 * (np.log(2*np.pi) + np.mean(np.log(vars) + np.square(y-mu)/vars))
    return ll
